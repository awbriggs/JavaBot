package listeners;

import static junit.framework.TestCase.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.impl.TextChannelImpl;

public class ChannelComparatorTest {

    @Test
    public void checkingOrderIsCorrectForComparator() {
        TextChannelImpl channel = new TextChannelImpl(0L, null);
        channel.setName("whatever");
        List<TextChannel> list = Arrays.asList(textChannelOfName("whatever"), textChannelOfName("aspen"), textChannelOfName("general"));
        list.sort(new ChannelComparator());

        assertEquals(list.get(0).getName(), "general");
        assertEquals(list.get(1).getName(), "aspen");
        assertEquals(list.get(2).getName(), "whatever");
    }

    private TextChannel textChannelOfName(String name) {
        TextChannelImpl channel = new TextChannelImpl(0L, null);
        channel.setName(name);

        return channel;
    }

}
