package commands.ping;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import commands.common.user.DefaultUser;
import commands.common.user.User;

public class PingTest {
    private Ping testObj;

    @Before
    public void setup() {
        testObj = new Ping();
    }

    @Test
    public void run() {
        assertEquals("pong", testObj.run(new DefaultUser("", "")));
    }

    @Test
    public void getStatsOfUser() {
        User user = new DefaultUser("123", "Austin");

        testObj.run(user);
        testObj.run(user);

        assertEquals("You have called this command 2 times", testObj.getStatsOfUser(user));
    }


}