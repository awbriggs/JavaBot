package commands.dice;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class DiceRollCommandTest {
    private DiceRollCommand testObj;
    private Random random;

    @Before
    public void setup() {
        random = mock(Random.class);
        testObj = new DiceRollCommand(random);
    }

    @Test
    public void roll() {
        when(random.nextInt(anyInt())).thenReturn(4);
        assertEquals("You have rolled a d6 for: 5", testObj.roll());
    }

    @Test
    public void rollXAmount() {
        when(random.nextInt(100)).thenReturn(23);
        assertEquals("You have rolled a d100 for: 24", testObj.rollSetDiceSideAmount("100"));
    }

}