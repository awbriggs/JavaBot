package commands.dice;

import java.security.SecureRandom;
import java.util.Random;

import commands.command.CommandMapping;
import commands.command.SubcommandMapping;

@CommandMapping("diceroll")
public class DiceRollCommand {
    private static final int DEFAULT_DICE_SIDES = 6;
    private final Random random;

    public DiceRollCommand() {
        random = new SecureRandom();
    }

    DiceRollCommand(Random random) {
        this.random = random;
    }

    @SubcommandMapping
    public String roll() {
        return getRollOfDice(DEFAULT_DICE_SIDES);
    }

    @SubcommandMapping(value = "\\d+$", pattern = true)
    public String rollSetDiceSideAmount(String input) {
        int diceSideAmount = Integer.parseInt(input);
        return getRollOfDice(diceSideAmount);
    }

    @SubcommandMapping("help")
    public String getHelp() {
        return "diceroll [dice to run amount]";
    }

    private String getRollOfDice(int sides) {
        return String.format("You have rolled a d%s for: %s", sides, random.nextInt(sides) + 1);
    }

}
