package commands.ping;

import java.util.HashMap;
import java.util.Map;

import commands.command.CommandMapping;
import commands.command.SubcommandMapping;
import commands.common.user.User;

@CommandMapping("ping")
public class Ping {
    private final Map<String, Integer> userIdToCallCount = new HashMap<>();

    @SubcommandMapping
    public String run(User callingUser) {
        incrementUserCallCount(callingUser);
        return "pong";
    }

    @SubcommandMapping("stats")
    public String getStatsOfUser(User callingUser) {
        Integer callCount = userIdToCallCount.getOrDefault(callingUser.getUserId(), 0);
        return String.format("You have called this command %s times", callCount);
    }

    private void incrementUserCallCount(User callingUser) {
        userIdToCallCount.compute(callingUser.getUserId(), (key, value) -> value == null ? 1 : value + 1);
    }

}
