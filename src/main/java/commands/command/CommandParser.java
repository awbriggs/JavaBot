package commands.command;

import java.util.Optional;

public class CommandParser {
    private final String contextVar;

    public CommandParser(String contextVar) {
        this.contextVar = contextVar;
    }

    public Optional<Command> parse(String command) {
        if (!contextVar.equals(command.substring(0, 1))) {
            return Optional.empty();
        }

        String[] commands = command.substring(1).split(" ");
        return commands.length == 2 ? Optional.of(new Command(commands[0], commands[1])) : Optional.of(new Command(commands[0]));
    }

    public String getContextVar() {
        return contextVar;
    }
}
