package commands.command;

public class Command {
    private final String command;
    private final String subCommand;

    public Command(String command) {
        this.command = command;
        this.subCommand = "";
    }

    public Command(String command, String subCommand) {
        this.command = command;
        this.subCommand = subCommand;
    }

    public String getCommand() {
        return command;
    }

    public String getSubCommand() {
        return subCommand;
    }

}
