package commands.command;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import commands.common.user.User;
import commands.common.Pair;

public class CommandRunner {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final Object instance;
    private final List<Pair<String, Method>> lookupBasicCommand;
    private final List<Pair<Pattern, Method>> lookupPatternCommand;

    public CommandRunner(Class<?> instance) {
        try {
            this.instance = instance.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException("Failed to make this");
        }
        this.lookupBasicCommand = loadStringSubCommand();
        this.lookupPatternCommand = loadPatternSubCommand();
    }

    public Optional<Object> getResultOfCommand(Command command, User user) {
        Optional<Pair<String, Method>> string = containsStrictlyEqualsSubCommand(command);
        if (string.isPresent()) {
            var commandMethod = string.get();
            try {
                var method = commandMethod.getValue();
                var args = new ArrayList<>();

                if (method.getParameterCount() == 1 && method.getParameters()[0].getType() == User.class) {
                    args.add(user);
                }

                return Optional.of(commandMethod.getValue().invoke(instance, args.toArray()));
            } catch (IllegalAccessException | InvocationTargetException e) {
                LOGGER.error("Error invoking command: ", e);
                return Optional.of("Error running command");
            }
        }

        Optional<Pair<Pattern, Method>> string1 = matchesPattern(command);

        if (string1.isPresent()) {
            var commandMethod = string1.get();
            try {
                var method = commandMethod.getValue();
                var args = new ArrayList<>();
                for (Parameter parameter : method.getParameters()) {
                    if (parameter.getType() == User.class) {
                        args.add(user);
                    } else if(parameter.getType() == String.class) {
                        args.add(command.getSubCommand());
                    }
                }

                return Optional.of(commandMethod.getValue().invoke(instance, args.toArray()));
            } catch (IllegalAccessException | InvocationTargetException e) {
                LOGGER.error("Error invoking command: ", e);
                return Optional.of("Error running command");
            }
        }

        return Optional.of("Unable to find command");
    }

    private List<Pair<String, Method>> loadStringSubCommand() {
        return Stream.of(instance.getClass().getMethods())
                .filter(this::methodIsAnnotatedWithSubCommand)
                .filter(method -> {
                    SubcommandMapping subcommandMapping = method.getAnnotation(SubcommandMapping.class);
                    return !subcommandMapping.pattern();
                })
                .map(method -> {
                    SubcommandMapping subcommandMapping = method.getAnnotation(SubcommandMapping.class);
                    return new Pair<>(subcommandMapping.value(), method);
                }).collect(Collectors.toList());
    }

    private List<Pair<Pattern, Method>> loadPatternSubCommand() {
        return Stream.of(instance.getClass().getMethods())
                .filter(this::methodIsAnnotatedWithSubCommand)
                .filter(method -> {
                    SubcommandMapping subcommandMapping = method.getAnnotation(SubcommandMapping.class);
                    return subcommandMapping.pattern();
                })
                .map(method -> {
                    SubcommandMapping subcommandMapping = method.getAnnotation(SubcommandMapping.class);
                    return new Pair<>(Pattern.compile(subcommandMapping.value()), method);
                }).collect(Collectors.toList());
    }

    private boolean methodIsAnnotatedWithSubCommand(Method method) {
        return method.getAnnotation(SubcommandMapping.class) != null;
    }

    private Optional<Pair<Pattern, Method>> matchesPattern(Command command) {
        return lookupPatternCommand.stream().filter(pair -> pair.getKey().matcher(command.getSubCommand()).matches()).findAny();
    }

    private Optional<Pair<String, Method>> containsStrictlyEqualsSubCommand(Command command) {
        return lookupBasicCommand.stream().filter(pair -> command.getSubCommand().equals(pair.getKey())).findAny();
    }

}
