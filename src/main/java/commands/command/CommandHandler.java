package commands.command;

import java.io.File;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import commands.common.user.DiscordUser;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class CommandHandler extends ListenerAdapter {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final CommandRegistry commandRegistry;
    private final CommandParser commandParser;

    public CommandHandler() {
        this.commandRegistry = new CommandRegistry();
        this.commandParser = new CommandParser("~");
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot() || event.getMessage().getAttachments().size() > 0) {
            return;
        }

        if (event.isFromType(ChannelType.PRIVATE)) {
            event.getChannel().sendMessage("Bot doesn't support private messages").queue();
            return;
        }

        Optional<Command> command = commandParser.parse(event.getMessage().getContentRaw());

        if (command.isEmpty()) {
            return;
        }

        sendCommandResult(event.getChannel(), commandRegistry.runCommand(command.get(), new DiscordUser(event.getAuthor())));
    }

    private void sendCommandResult(MessageChannel messageChannel, Object object) {
        if (object == null) {
            return;
        } else if (object instanceof String) {
            messageChannel.sendMessage((String) object).queue();
        } else if (object instanceof File) {
            messageChannel.sendFile((File) object).queue();
        } else if (object instanceof Iterable) {
            Iterable<Object> objectIterable = (Iterable<Object>) object;
            objectIterable.forEach(item -> sendCommandResult(messageChannel, item));
        } else {
            LOGGER.warn("Invalid type used for result: {}. Sending toString of object", object.getClass());
            messageChannel.sendMessage(object.toString()).queue();
        }
    }
}
