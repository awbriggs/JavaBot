package commands.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.reflections.Reflections;

import commands.common.user.User;

public class CommandRegistry {
    public static final String COMMAND_PACKAGES = "commands";
    private final Map<String, CommandRunner> commandToClassMap = new HashMap<>();

    public CommandRegistry() {
        loadAllCommands();
    }

    public Object runCommand(Command command, User user) {
        return getCommand(command).get().getResultOfCommand(command, user).orElse("No result returned");
    }

    private Optional<CommandRunner> getCommand(Command command) {
        return commandToClassMap.containsKey(command.getCommand()) ? Optional.of(commandToClassMap.get(command.getCommand())) : Optional.empty();
    }

    private void loadAllCommands() {
        var reflectionCommandPackage = new Reflections(COMMAND_PACKAGES);
        var annotatedClasses = reflectionCommandPackage.getTypesAnnotatedWith(CommandMapping.class);
        annotatedClasses.forEach(clazz -> commandToClassMap.put(clazz.getAnnotation(CommandMapping.class).value(), new CommandRunner(clazz)));
    }

}
