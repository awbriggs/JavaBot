package commands.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SubcommandMapping {
    /**
     * Register what this should run with. Empty is the default for when the command runs.
     * Example:
     *
     * @Run("test") Will register under the command + "test".
     */
    String value() default "";

    boolean pattern() default false;
}
