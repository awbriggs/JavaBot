package commands.urlshortener;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

import org.json.JSONObject;

import commands.command.CommandMapping;
import commands.command.SubcommandMapping;

@CommandMapping("shorten")
public class UrlShortenCommand {
    private final HttpClient httpClient;

    public UrlShortenCommand() {
        httpClient = HttpClient.newHttpClient();
    }

    public UrlShortenCommand(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @SubcommandMapping(value = "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,})", pattern = true)
    public String shortenUrl(String match) {
        Optional<String> result = getShortenedUrl(match);

        if (result.isEmpty()) {
            return "Error shortening the url. Maybe my fault, probably your fault";
        }
        return "New Url shortened url: " + result.get() + " this link should be valid for one hour.";
    }

    @SubcommandMapping("help")
    public String getHelp() {
        return "shorten [url]";
    }

    private Optional<String> getShortenedUrl(String url) {
        try {
            HttpRequest request = HttpRequest.newBuilder(URI.create("https://link.austinbriggs.dev/link"))
                    .POST(HttpRequest.BodyPublishers.ofString(buildUrlRequest(url)))
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build();

            HttpResponse<String> httpResponse = this.httpClient
                    .send(request, HttpResponse.BodyHandlers.ofString());

            JSONObject jsonObject = new JSONObject(httpResponse.body());

            return Optional.of(jsonObject.getString("newUrl"));
        } catch (IOException | InterruptedException ex) {
            return Optional.empty();
        }
    }

    private String buildUrlRequest(String url) {
        JSONObject requestBody = new JSONObject();
        requestBody.put("oldUrl", url);
        requestBody.put("minutesToExist", 60);

        return requestBody.toString();
    }
}
