package commands.common.user;

public class DiscordUser extends User {
    private final net.dv8tion.jda.core.entities.User user;

    public DiscordUser(net.dv8tion.jda.core.entities.User user) {
        this.user = user;
    }

    @Override
    public String getUserId() {
        return user.getId();
    }

    @Override
    public String getUserName() {
        return user.getName();
    }

}
