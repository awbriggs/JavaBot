package commands.common.user;

public abstract class User {
    abstract public String getUserId();
    abstract public String getUserName();
}
