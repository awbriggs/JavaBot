package commands.common.user;

public class DefaultUser extends User {
    private final String userId;
    private final String username;

    public DefaultUser(String userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getUserName() {
        return username;
    }

}
