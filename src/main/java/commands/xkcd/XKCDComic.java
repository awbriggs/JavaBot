package commands.xkcd;

import java.io.File;

class XKCDComic {
    private final File imageFile;
    private final String altText;
    private final String title;

    XKCDComic(File imageFile, String altText, String title) {
        this.imageFile = imageFile;
        this.altText = altText;
        this.title = title;
    }

    public File getImage() {
        return this.imageFile;
    }

    public String getAltText() {
        return this.altText;
    }

    public String getTitle() {
        return this.title;
    }
}