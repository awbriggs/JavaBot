package commands.xkcd;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import commands.command.CommandMapping;
import commands.command.SubcommandMapping;

@CommandMapping("xkcd")
public class Xkcd {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @SubcommandMapping
    public List<Object> getLatest() {
        XKCDComic image = getLatestComic();
        return image == null ? Collections.singletonList("Failure retrieving image") : List.of(image.getTitle(), image.getImage(), image.getAltText());
    }

    @SubcommandMapping("random")
    public List<Object> getRandom() {
        XKCDComic image = getRandomComic();
        return image == null ? Collections.singletonList("Failure to retrieve comic") : List.of(image.getTitle(), image.getImage(), image.getAltText());
    }

    @SubcommandMapping("help")
    public String getHelp() {
        return "xkcd -help -random";
    }

    private XKCDComic getLatestComic() {
        return getComic("https://www.xkcd.com");
    }

    private XKCDComic getRandomComic() {
        return getComic("https://c.xkcd.com/random/comic/");
    }

    private XKCDComic getComic(String comicEndpoint) {
        String imageLink = null;
        String altText = null;
        String title = null;
        Document doc;

        try {
            doc = Jsoup.connect(comicEndpoint).get();
            Elements picture = doc.select("img[src$=.png]");
            for (Element element : picture) {
                if (element.attr("abs:src").contains("comics")) {
                    imageLink = element.attr("abs:src");
                    altText = element.attr("title");
                    title = element.attr("alt");
                }
            }
            if (imageLink != null) {
                URL url = new URL(imageLink);
                try (InputStream in = new BufferedInputStream(url.openStream());
                        OutputStream out = new BufferedOutputStream(new FileOutputStream("/tmp/xkcd.png"))) {
                    out.write(in.readAllBytes());

                } catch (IOException ex) {
                    LOGGER.warn("Error fetching and writing exception: ", ex);
                }
                return new XKCDComic(new File("/tmp/xkcd.png"), altText, title);
            }
        } catch (IOException e) {
            LOGGER.warn("Error fetching and writing exception: ", e);
        }
        return null;
    }

}

