package utils;

import java.util.regex.Pattern;

public enum TextPatterns {
    LINK("(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,})"),
    COMMAND(":(.*?):");

    private Pattern pattern;
    private String regexPattern;

    TextPatterns(String regex) {
        this.pattern = Pattern.compile(regex);
        this.regexPattern = regex;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getStringPattern() {
        return regexPattern;
    }

}
