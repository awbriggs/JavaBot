package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

public class Utils {
    private final static Logger log = LoggerFactory.getLogger(Utils.class);

    public static String getEnvVar(String envVar, boolean fail){
        String var = System.getenv(envVar);
        if (var == null){
            System.err.println(String.format("Environment variable: (%s) not set", envVar));
            if (fail){
                System.exit(1);
            }
        }
        return var;
    }

    public static String getMessageTimeDifference(Message first, Message second){
        long secondsDifference = second.getCreationTime().toEpochSecond() - first.getCreationTime().toEpochSecond();

        if(secondsDifference / 86400 >= 1){
            return String.format("%d days", secondsDifference / 86400);
        } else if(secondsDifference / 3600 >= 1){
            return String.format("%d hours", secondsDifference / 3600);
        } else if(secondsDifference / 60 >= 1){
            return String.format("%d minutes", secondsDifference / 60);
        }else{
            return String.format("%d seconds", secondsDifference);
        }
    }

    public static void logText(MessageReceivedEvent event, String found, String result){
        log.info("[{}][{}] Found: {} Result: {}\n", event.getGuild().getName(), event.getTextChannel().getName(), found, result);
    }

    public static int getAmountOfPeopleInVoice(GuildVoiceJoinEvent event) {
        return event.getGuild().getVoiceChannels().stream().mapToInt(channel -> {
            if (!(channel == event.getGuild().getAfkChannel())) {
                return channel.getMembers().size();
            }
            return 0;
        }).sum();
    }

}
