import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import commands.command.CommandHandler;
import listeners.ChannelMovedListener;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

public class JavaBot {

    private final static Logger LOGGER = LoggerFactory.getLogger(JavaBot.class);

    public static void main(String...args) {
        try {
            JDA jda = new JDABuilder(AccountType.BOT)
                    .setToken(System.getenv("token"))
                    .build();
            jda.addEventListener(new CommandHandler(), new ChannelMovedListener());
            LOGGER.info("Logged in as: {}", jda.getSelfUser());
        } catch (LoginException e) {
            LOGGER.error("Failure logging in", e);
        }
    }

}
