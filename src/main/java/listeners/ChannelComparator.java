package listeners;

import java.util.Comparator;

import net.dv8tion.jda.core.entities.TextChannel;

class ChannelComparator implements Comparator<TextChannel> {
    // Top channel at all times
    private static final String DEFAULT_TOP_CHANNEL = "general";

    @Override
    public int compare(TextChannel channelOne, TextChannel channelTwo){
        if (channelOne == channelTwo) {
            return 0;
        } else if (channelOne.getName().equals(DEFAULT_TOP_CHANNEL)) {
            return -1;
        } else if (channelTwo.getName().equals(DEFAULT_TOP_CHANNEL)) {
            return 1;
        }

        return channelOne.getName().compareTo(channelTwo.getName());
    }

}
