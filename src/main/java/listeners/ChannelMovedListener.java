package listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.channel.text.update.TextChannelUpdatePositionEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.requests.restaction.order.ChannelOrderAction;

public class ChannelMovedListener extends ListenerAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelMovedListener.class);

    @Override
    public void onTextChannelUpdatePosition(TextChannelUpdatePositionEvent event){
        LOGGER.info("{} {} Channel was moved. Moving it back\n", event.getGuild().getName(), event.getChannel().getName());

        new ChannelOrderAction<TextChannel>(event.getGuild(), ChannelType.TEXT)
                .sortOrder(new ChannelComparator())
                .queue();
    }

}


